<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class IndexController extends AppController
{

    public function index(){
      $this->generales = (new Generales())->find_first();
      $this->sliders = (new Sliders())->find();
      $this->project = (new Project())->find_first();
      $this->all_imgs = (new Galeria())->find("order: id");
      $this->amenities = (new Amenities())->find_first();
      $this->ubicacion = (new Ubicacion())->find_first();

      $dzEmailTo = "grupo.gabrava@gmail.com";
      $dzEmailFrom   = "Warehouse Contacto";
      if(!empty($_POST) && $_POST['dzToDo'] == 'Contacto'){
      	$dzName = trim(strip_tags($_POST['dzName']));
      	$dzEmail = trim(strip_tags($_POST['dzEmail']));
      	$dzMessage = strip_tags($_POST['dzMessage']);
      	$dzIntereses = strip_tags($_POST['dzIntereses']);
      	$dzTelefonos = strip_tags($_POST['dzTelefonos']);

      	$dzMailSubject = 'Mensaje - Warehouse Contacto';
      	$dzMailMessage	= 	"
      						Una persona te ha contactado: <br><br>
      						Nombre: $dzName<br/>
      						Email: $dzEmail<br/>
      						Intereses: $dzIntereses<br/>
      						Telefonos: $dzTelefonos<br/>
      						Mensaje: $dzMessage<br/>
      						";
      	$dzEmailHeader = "MIME-Version: 1.0\r\n";
      	$dzEmailHeader .= "Content-type: text/html; charset=iso-8859-1\r\n";
      	$dzEmailHeader .= "From:$dzEmailFrom <$dzEmail>";
      	$dzEmailHeader .= "Reply-To: $dzEmail\r\n"."X-Mailer: PHP/".phpversion();
      	if(mail($dzEmailTo, $dzMailSubject, $dzMailMessage, $dzEmailHeader)){
          Flash::valid('Mensaje enviado satisfactoriamente. Gracias por contactarnos');
      	}else{
          Flash::error('Ocurrio un problema. Por favor intenta nuevamente');
      	}
      }


    }
}
