<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class EnController extends AppController{

  public function index(){
    View::template('en');
    $this->generales = (new Generales())->find_first();
    $this->sliders = (new Sliders())->find();
    $this->project = (new Project())->find_first();
    $this->all_imgs = (new Galeria())->find("order: id");
    $this->amenities = (new Amenities())->find_first();
    $this->ubicacion = (new Ubicacion())->find_first();

    $dzEmailTo = "adm.gabrava@gmail.com";
    $dzEmailFrom   = "Warehouse Contact";

    if(!empty($_POST) && $_POST['dzToDo'] == 'Contacto'){
    	$dzName = trim(strip_tags($_POST['dzName']));
    	$dzEmail = trim(strip_tags($_POST['dzEmail']));
    	$dzMessage = strip_tags($_POST['dzMessage']);
    	$dzIntereses = strip_tags($_POST['dzIntereses']);
    	$dzTelefonos = strip_tags($_POST['dzTelefonos']);

    	$dzMailSubject = 'Message - Warehouse Contact';
    	$dzMailMessage	= 	"
    						A person has contacted you: <br><br>
    						Full Name: $dzName<br/>
    						Email: $dzEmail<br/>
    						Interests: $dzIntereses<br/>
    						Phone number: $dzTelefonos<br/>
    						Message: $dzMessage<br/>
    						";
    	$dzEmailHeader = "MIME-Version: 1.0\r\n";
    	$dzEmailHeader .= "Content-type: text/html; charset=iso-8859-1\r\n";
    	$dzEmailHeader .= "From:$dzEmailFrom <$dzEmail>";
    	$dzEmailHeader .= "Reply-To: $dzEmail\r\n"."X-Mailer: PHP/".phpversion();
    	if(mail($dzEmailTo, $dzMailSubject, $dzMailMessage, $dzEmailHeader)){
        Flash::valid('Message sent successfully. Thank you for contact us');
    	}else{
        Flash::error('There is been a problem. Please try again');
    	}
    }

  }
}
