<?php

/**
 * Controller por defecto si no se usa el routes
 *
 */
class AdminController extends AppController
{

    public function index(){
      if(!Auth::is_valid()){
        return Redirect::to('admin/login/');
      }
      return Redirect::to('admin/general/');
    }

    public function logout(){
      Auth::destroy_identity();
      return Redirect::to('admin/login');
    }

    public function general(){
      View::template('admin');
      if(!Auth::is_valid()){
        return Redirect::to('admin/login/');
      }
      if(Input::hasPost('generales')){
        $this->generales = (new Generales(Input::post('generales')));
        if($this->generales->update()){
          Flash::valid("Actualizado!");
        }
      }else{
        $this->generales = (new Generales())->find_first();
      }
    }

    public function login()
    {
      if(Auth::is_valid()){
        return Redirect::to('admin');
      }
      View::template('login');
      if(Input::hasPost('users')){
        $pwd = Input::post('users.password');
        $username = Input::post('users.username');
        $auth = new Auth("model", "class: users", "username: $username", "password: $pwd");
        if ($auth->authenticate()) {
          return Redirect::to("admin");
        }else{
          Flash::error('Usuario o contraseña incorrecta!');
        }
      }
    }
    public function sliders(){
      View::template('admin');
      if(!Auth::is_valid()){
        return Redirect::to('admin/login/');
      }
      $this->sliders = (new Sliders())->find();
    }

    public function agregar_sliders(){
      View::template('admin');
      if(!Auth::is_valid()){
        return Redirect::to('admin/login/');
      }
      if (Input::haspost("sliders")) {
        $sliders = new Sliders(Input::post("sliders"));
        $path = getcwd()."/img/upload/sliders/";
        if (!empty($_FILES['imagen']['name'])) {
            $images = Upload::factory('imagen', 'image');
            $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
            $images->setPath($path);
            $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
            if ($images->isUploaded()) {
                if ($images->save()){
                    $sliders->imagen = "upload/sliders/".$_FILES["imagen"]["name"];
                }else{
                Flash::warning('No se ha podido subir la imagen ...!!!');
               }
             }
         }
        if ($sliders->save()) {
          Flash::valid("Slider Registrado");
          return Redirect::to('admin/sliders/');
        }else{
          Flash::error("Error al registrar el blog");
        }
      }
    }

    public function editar_slider($id){
      View::template("admin");
      if (Auth::is_valid()){
        if (Input::haspost("sliders")) {
          $sliders = new Sliders(Input::post("sliders"));
          $path = getcwd()."/img/upload/sliders/";

          if(!empty($_FILES['imagen']['name'])) {
              $images = Upload::factory('imagen', 'image');
              $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
              $images->setPath($path);
              $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
                  if ($images->isUploaded()) {
                      if ($images->save()){
                          $sliders->imagen = "upload/sliders/".$_FILES["imagen"]["name"];
                      }else{
                      Flash::warning('No se ha podido subir la imagen ...!!!');
                   }
                 }
          }

          if ($sliders->update()) {
            Flash::valid("Datos actualizados");
            return Redirect::to('admin/sliders/');
          }else{
            Flash::error("Error al editar, verifique e intente de nuevo");
            return Redirect::to('admin/editar_slider/$id/');
          }
        }else {
          $sliders = new Sliders();
          $this->sliders = $sliders->find_by_id((int)$id);
        }
      }else{
        Flash::valid("Necesita un usuario autenticado");
        return Redirect::to('admin/');
      }
    }

    public function eliminar_slider($id){
    if (Auth::is_valid()){
       $sliders = new Sliders();
       $sliders = $sliders->find_by_id($id);

       if ($sliders->delete((int)$id)) {
           Flash::valid('Operación exitosa');
       }else{
           Flash::error('Falló Operación');
       }
       return Redirect::to('admin/sliders/');
    }else{
       Flash::valid("Necesita un usuario autenticado");
       return Redirect::to('admin/');
     }
   }

   public function amenities() {
     View::template('admin');
     if(!Auth::is_valid()){
       return Redirect::to('admin/login/');
     }
     if(Input::hasPost('amenities')){
       $this->amenities = (new Amenities(Input::post('amenities')));
       if($this->amenities->update()){
         Flash::valid("Actualizado!");
       }
     }else{
       $this->amenities = (new Amenities())->find_first();
     }
   }

   public function project() {
     View::template('admin');
     if(!Auth::is_valid()){
       return Redirect::to('admin/login/');
     }
     if(Input::hasPost('project')){
       $this->project = (new Project(Input::post('project')));
       if(!empty($_FILES['imagen']['name'])) {
           $path = getcwd()."/img/upload/project/";
           $images = Upload::factory('imagen', 'image');
           $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
           $images->setPath($path);
           $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
               if ($images->isUploaded()) {
                   if ($images->save()){
                       $this->project->imagen = "upload/project/".$_FILES["imagen"]["name"];
                   }else{
                   Flash::warning('No se ha podido subir la imagen ...!!!');
                }
              }
       }
       if($this->project->update()){
         Flash::valid("Actualizado!");
       }
     }else{
       $this->project = (new Project())->find_first();
     }
  }

  public function galeria(){
    View::template('admin');
    if(!Auth::is_valid()){
      return Redirect::to('admin/login/');
    }
    $this->galeria = (new Galeria())->find();
  }

  public function eliminar_galeria($id){
  if (Auth::is_valid()){
     $galeria = new Galeria();
     $galeria = $galeria->find_by_id($id);

     if ($galeria->delete((int)$id)) {
         Flash::valid('Operación exitosa');
     }else{
         Flash::error('Falló Operación');
     }
     return Redirect::to('admin/galeria/');
  }else{
     Flash::valid("Necesita un usuario autenticado");
     return Redirect::to('admin/');
   }
 }

  public function agregar_galeria(){
    View::template('admin');
    if(!Auth::is_valid()){
      return Redirect::to('admin/login/');
    }
    if (Input::haspost("galeria")) {
      $galeria = new Galeria(Input::post("galeria"));
      $path = getcwd()."/img/upload/galeria/";
      if (!empty($_FILES['imagen']['name'])) {
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
          if ($images->isUploaded()) {
              if ($images->save()){
                  $galeria->imagen = "upload/galeria/".$_FILES["imagen"]["name"];
              }else{
                Flash::warning('No se ha podido subir la imagen ...!!!');
              }
           }
       }
      if ($galeria->save()) {
        Flash::valid("Imagen de Galeria Registrada");
        return Redirect::to('admin/galeria/');
      }else{
        Flash::error("Error al registrar la imagen de galeria");
      }
    }
  }

  public function editar_galeria($id){
    View::template("admin");
    if (Auth::is_valid()){
      if (Input::haspost("galeria")) {
        $galeria = new Galeria(Input::post("galeria"));
        $path = getcwd()."/img/upload/galeria/";

        if(!empty($_FILES['imagen']['name'])) {
            $images = Upload::factory('imagen', 'image');
            $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
            $images->setPath($path);
            $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
                if ($images->isUploaded()) {
                    if ($images->save()){
                        $galeria->imagen = "upload/galeria/".$_FILES["imagen"]["name"];
                    }else{
                    Flash::warning('No se ha podido subir la imagen ...!!!');
                 }
               }
        }

        if ($galeria->update()) {
          Flash::valid("Datos actualizados");
          return Redirect::to('admin/galeria/');
        }else{
          Flash::error("Error al editar, verifique e intente de nuevo");
          return Redirect::to('admin/editar_galeria/'.$id.'/');
        }
      }else {
        $galeria = new Galeria();
        $this->galeria = $galeria->find_by_id((int)$id);
      }
    }else{
      Flash::valid("Necesita un usuario autenticado");
      return Redirect::to('admin/');
    }
  }

  public function ubicacion(){
    View::template('admin');
    if(!Auth::is_valid()){
      return Redirect::to('admin/login/');
    }
    if(Input::hasPost('ubicacion')){
      $this->ubicacion = (new Ubicacion(Input::post('ubicacion')));
      if(!empty($_FILES['imagen']['name'])) {
          $path = getcwd()."/img/upload/ubicacion/";
          $images = Upload::factory('imagen', 'image');
          $_FILES["imagen"]["name"] = date("Y_m_d_h.i.s").$_FILES['imagen']['name'];
          $images->setPath($path);
          $images->setExtensions(array('jpg', 'png', 'gif','jpeg'));
              if ($images->isUploaded()) {
                  if ($images->save()){
                      $this->ubicacion->imagen = "upload/ubicacion/".$_FILES["imagen"]["name"];
                  }else{
                  Flash::warning('No se ha podido subir la imagen ...!!!');
               }
             }
      }
      if($this->ubicacion->update()){
        Flash::valid("Actualizado!");
      }
    }else{
      $this->ubicacion = (new Ubicacion())->find_first();
    }
  }

}
